package pl.jacek.mp3player.player;
import org.junit.jupiter.api.*;
import java.io.*;
import pl.jacek.mp3player.mp3.Mp3Song;

import static org.junit.jupiter.api.Assertions.*;

class Mp3ParserTest {

    private File okFile;
    private File wrongFile;
    private File noTagsFile;
    private Mp3Song okMp3Song;
    private Mp3Song noTagsMp3Song;
    private Mp3Song wrongMp3Song;

    @BeforeAll
    public  void setUp(){
         okFile = new File("okSong.mp3");
         wrongFile = new File("wrongFile.jpeg");
        noTagsFile = new File("noTagsSong.mp3");


    }
    @Test
    void shouldCreateOkSong() {
    try {
            okMp3Song = Mp3Parser.createSong(okFile);

        }catch (java.io.IOException| org.farng.mp3.TagException e){

        e.printStackTrace();
        fail("Rzucono wyjątek");
        }

//////____________
    }
    @Test
    void crateSongwithoutTags() {
        try {
            noTagsMp3Song = Mp3Parser.createSong(noTagsFile);

        }catch (java.io.IOException| org.farng.mp3.TagException e){

            e.printStackTrace();
            fail("Rzucono wyjątek");
        }
        Assertions.assertNotNull(noTagsMp3Song);

    }


    @Test
    void checkTagsinMP3withNoTags() {

        try {
            noTagsMp3Song = Mp3Parser.createSong(noTagsFile);

        }catch (java.io.IOException| org.farng.mp3.TagException e){

            e.printStackTrace();
            fail("Rzucono wyjątek");
        }
        Assertions.assertNotNull(noTagsMp3Song);

    }
    @Test
    void createWrongSong() {

        Assertions.assertThrows(java.io.IOException.class, () -> {
            wrongMp3Song = Mp3Parser.createSong(wrongFile);
        });


    }



    }