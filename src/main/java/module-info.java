module mp3player {
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.media;
    requires jdk.internal.vm.compiler;
    requires javafx.base;

    exports pl.jacek.mp3player.main to javafx.graphics;
    requires jid3lib;

    opens  pl.jacek.mp3player.controller to javafx.fxml;
    opens pl.jacek.mp3player.mp3 to jdk.internal.vm.compiler, javafx.base;


}