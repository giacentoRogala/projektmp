package pl.jacek.mp3player.player;

import javafx.collections.*;
import pl.jacek.mp3player.mp3.*;
import javafx.scene.media.*;
import javafx.scene.media.MediaPlayer.*;
import java.io.*;

public class Mp3Player {

    private Media media;
    private MediaPlayer mediaPlayer;
    private ObservableList<Mp3Song> songList;
    private ObservableList<Mp3Song> historySongList;


    public Mp3Player(ObservableList<Mp3Song> songList) {

        this.songList = songList;

    }
    public MediaPlayer getMediaPlayer(){

        return mediaPlayer;
    }

    public void play(){

        if(mediaPlayer!=null && (mediaPlayer.getStatus()== Status.READY||mediaPlayer.getStatus() == Status.PAUSED)){
            mediaPlayer.play();

        }
    }

    public void stop() {
        if (mediaPlayer != null && mediaPlayer.getStatus() == Status.PLAYING) {
            mediaPlayer.stop();

        }
    }
    public void pause() {
        if (mediaPlayer != null && mediaPlayer.getStatus() == Status.PLAYING) {
            mediaPlayer.pause();

        }
    }

        public double getSongLength(){
            if(media!=null){

                return media.getDuration().toSeconds();
            }else
                return 0;

        }

        public ObservableList<Mp3Song> getSongList(){

            return songList;
        }

        public void setVolume(double volume) {

            if (mediaPlayer != null) {
                mediaPlayer.setVolume(volume);

            }
        }

        public void loadSong(int index){

            media = new Media(new File(songList.get(index).getFilePath()).toURI().toString());
            mediaPlayer = new MediaPlayer(media);
            getMediaPlayer().statusProperty().addListener((observable, oldStatus, newStatus) -> {
                if (newStatus == MediaPlayer.Status.READY)
                    mediaPlayer.setAutoPlay(true);


            });


            }

        }




