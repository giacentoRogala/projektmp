package pl.jacek.mp3player.player;
import org.farng.mp3.MP3File;

import org.farng.mp3.TagException;
import pl.jacek.mp3player.mp3.*;
import  java.io.*;
import java.io.IOException;
import java.util.*;
import javafx.collections.*;
import javafx.scene.control.*;

public class Mp3Parser {

   public static Mp3Song createSong(File file) throws IOException, TagException {
       String title="";
       String author="";
       String album="";
       String absolutePath="";

       try {
           MP3File mp3file = new MP3File(file);
    if(mp3file.hasID3v2Tag()) {

            title = mp3file.getID3v2Tag().getLeadArtist();

            author = mp3file.getID3v2Tag().getLeadArtist();

            album = mp3file.getID3v2Tag().getAlbumTitle();


    }else{
        title = file.getName();
    }

           absolutePath = file.getAbsolutePath();
       } catch (java.io.IOException e) {
           e.printStackTrace();
       } catch (org.farng.mp3.TagException e) {
           e.printStackTrace();
       }
       return new Mp3Song(title, author, album, absolutePath);
   }



   public static List<Mp3Song> createMp3SongList(File dir)throws IOException, TagException, UnsupportedOperationException {

    if(!dir.isDirectory()) {
        throw new IllegalArgumentException();
    }
    List<Mp3Song> songList = new ArrayList<>();
    File[] files = dir.listFiles();

    for(File file : files){

    String fileExtension = file.getName().substring(file.getName().lastIndexOf(".")+1);
    if (fileExtension.equals("mp3"))
        songList.add(createSong(file));

    }

    return songList;

    }

   public static void saveHistory( List<Mp3Song> playHistory) {


        PrintStream printHistory = null;
        try {
            File historyFile = new File("history.txt");
            if(!historyFile.exists())
                historyFile.createNewFile();
            printHistory = new PrintStream(historyFile);


            for (Mp3Song mp3 : playHistory) {
                printHistory.println(mp3.getFilePath());

            }

        } catch (IOException e) {

            e.printStackTrace();


        } finally {
            if(printHistory!=null)
            printHistory.close();
        }
    }


   public static void updateHistory(TableView<pl.jacek.mp3player.mp3.Mp3Song> historyTable, LinkedList <Mp3Song> playHistory, ObservableList<Mp3Song> historySonglist, ObservableList<Mp3Song> items, int index){
    if(playHistory.contains(items.get(index)))
        playHistory.remove(items.get(index));
    playHistory.addFirst(items.get(index));
       while(playHistory.size()>=10) {
           playHistory.removeLast();

       }
    historySonglist.setAll(playHistory);
    historyTable.setItems(historySonglist);



   }


   public static void readHistory(List<Mp3Song> playHistory) {

       String thisLine;
       BufferedReader historyReader;
       Mp3Song mp3Song = null;
       try {
           historyReader = new BufferedReader(new FileReader("history.txt"));
           playHistory.clear();
           while ((thisLine = historyReader.readLine()) != null) {
               mp3Song = createMp3SongFromPath(thisLine);

               playHistory.add(mp3Song);
           }
       } catch (IOException e) {

           e.printStackTrace();
       }

   }

    public static Mp3Song createMp3SongFromPath (String filePath){
           java.io.File file = new java.io.File(filePath);
            String title="";
            String author="";
            String album="";
            String absolutePath;

            if(file.exists()) {


                try {
                    MP3File mp3 = new MP3File(file);
                    if(mp3.hasID3v2Tag()) {

                        title = mp3.getID3v2Tag().getSongTitle();
                        album = mp3.getID3v2Tag().getAlbumTitle();
                        author = mp3.getID3v2Tag().getLeadArtist();
                    }else{
                    title = file.getName();
                }
                    absolutePath = file.getAbsolutePath();

                    return new Mp3Song(title, author, album, absolutePath);

                } catch (java.io.IOException | org.farng.mp3.TagException o) {

                    o.printStackTrace();
                    return null;


                }
            }

        return null;
       }



    }





