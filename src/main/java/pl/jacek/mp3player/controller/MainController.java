package pl.jacek.mp3player.controller;

import javafx.fxml.*;
import pl.jacek.mp3player.player.*;
import pl.jacek.mp3player.mp3.*;
import javafx.collections.*;
import javafx.scene.input.*;
import  javafx.scene.control.*;
import javafx.util.*;
import java.util.*;
import javafx.collections.transformation.*;
import javafx.beans.binding.*;
import javafx.beans.value.*;
import  javafx.stage.*;
import java.io.*;
import java.text.*;
import static java.lang.Boolean.TRUE;
import static pl.jacek.mp3player.player.Mp3Parser.*;


public class MainController {


    @FXML
    private ControlPaneController controlPaneController;
    @FXML
    private MenuPaneController menuPaneController;
    @FXML
    private ContentPaneController contentPaneController;
    @FXML
    private SearchPaneController searchPaneController;

    private Mp3Player mp3Player;
    private  ObservableList<Mp3Song> items;
    private  ObservableList<Mp3Song> historyItems;
    private int lastIndex =-1;
    private LinkedList<Mp3Song> playHistory;

    public void initialize() {

        createPlayer();
        configureMenu();
        configureTable();
        configureButtons();
        configureSearchingPane();
        configureHistoryTable();


    }

    private void createPlayer(){
        ObservableList<Mp3Song> songList = contentPaneController.getLabelsTable().getItems();
        items = contentPaneController.getLabelsTable().getItems();
        mp3Player = new Mp3Player(songList);

        playHistory = new LinkedList<>();
    }

    private void configureTable(){

        items = contentPaneController.getLabelsTable().getItems();
        contentPaneController.getLabelsTable().addEventHandler(MouseEvent.MOUSE_CLICKED, event ->
        {if(event.getClickCount()==2){
           mp3Player.stop();
            lastIndex = contentPaneController.getLabelsTable().getSelectionModel().getSelectedIndex();
            playSong(lastIndex);
            contentPaneController.getLabelsTable().addEventHandler(	javafx.scene.input.KeyEvent.KEY_RELEASED, keyEvent ->{
            if(keyEvent.getCode() == javafx.scene.input.KeyCode.DELETE){
                try{
                    items.remove(contentPaneController.getLabelsTable().getSelectionModel().getSelectedItem());

                }catch(NullPointerException e){

                    e.printStackTrace();
                }

            }

            });
        }});


    }

    private void configureTimeSlider(){

    Slider timeSlider = controlPaneController.getTimeSlider();
    Label timeLabel = controlPaneController.getTimeLabel();


        timeSlider.valueProperty().addListener((observable, oldValue, newValue)->{
            if(timeSlider.isValueChanging()) {
                Thread timeThread = new Thread(()->{
                mp3Player.getMediaPlayer().seek(Duration.seconds(newValue.doubleValue()));
            });
            timeThread.start();
            }
        });

        DateFormat timeValue = new SimpleDateFormat("hh:mm:ss");
        mp3Player.getMediaPlayer().setOnReady(()->{timeSlider.setMax(mp3Player.getSongLength());
            mp3Player.getMediaPlayer().currentTimeProperty().addListener((arg, oldValue,newValue)->
            {timeSlider.setValue(newValue.toSeconds());
                timeLabel.setText(timeValue.format(newValue.toMillis()));
            });});}

    private void configureVolume(){

        Slider volumeSlider = controlPaneController.getVolumeSlider();
        volumeSlider.valueProperty().unbind();

        volumeSlider.valueProperty().bindBidirectional(mp3Player.getMediaPlayer().volumeProperty());

        }

    private void configureButtons(){

        TableView<Mp3Song> labelsTable = contentPaneController.getLabelsTable();
        ToggleButton playButton = controlPaneController.getPlayButton();
        Button prevButton = controlPaneController.getPreviousButton();
        Button nextButton = controlPaneController.getNextButtonButton();
        Slider timeSlider = controlPaneController.getTimeSlider();


        playButton.setOnAction(event ->
        {
        if(labelsTable.getSelectionModel().getSelectedItem()!=null) {
            if (playButton.isSelected()) {
                if(lastIndex!=labelsTable.getSelectionModel().getSelectedIndex()){
                    lastIndex = labelsTable.getSelectionModel().getSelectedIndex();
                    playSong(lastIndex);
                }
                else
                    mp3Player.play();

            }
            else{
                if(lastIndex!=labelsTable.getSelectionModel().getSelectedIndex()){

                    lastIndex=labelsTable.getSelectionModel().getSelectedIndex();
                    mp3Player.stop();
                    playSong(lastIndex);

                    playButton.setSelected(true);
                }else
                    mp3Player.pause();

               }
            lastIndex =labelsTable.getSelectionModel().getSelectedIndex();




        }else
            playButton.setSelected(false);

        });

        nextButton.setOnAction(event ->
        { labelsTable.getSelectionModel().select(labelsTable.getSelectionModel().getSelectedIndex() + 1);
            if(playButton.isSelected()) {
                lastIndex = labelsTable.getSelectionModel().getSelectedIndex();
                mp3Player.stop();
                playSong(lastIndex);
        }

        });
        prevButton.setOnAction(event ->
        {   labelsTable.getSelectionModel().select(labelsTable.getSelectionModel().getSelectedIndex() - 1);
            if(playButton.isSelected()) {

                lastIndex = labelsTable.getSelectionModel().getSelectedIndex();
                mp3Player.stop();
                playSong(lastIndex);

            }
        });




    }

    private void configureSearchingPane() {

        final List<TableColumn<Mp3Song, ?>> columns = contentPaneController.getLabelsTable().getColumns();
        FilteredList<Mp3Song> filteredData = new FilteredList<>(mp3Player.getSongList());
        filteredData.predicateProperty().bind(Bindings.createObjectBinding(() -> {
            String text = searchPaneController.getSearchField().getText();

            if (text == null || text.isEmpty()) {
                return null;
            }
            final String filterText = text.toLowerCase();

            return o -> {
                for (TableColumn<Mp3Song, ?> col : columns) {
                    ObservableValue<?> observable = col.getCellObservableValue(o);
                    if (observable != null) {
                        Object value = observable.getValue();
                        if (value != null && value.toString().toLowerCase().contains(filterText)) {
                            return true;
                        }
                    }
                }
                return false;
            };
        }
        ,searchPaneController.getSearchField().textProperty()));
        SortedList<Mp3Song> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(contentPaneController.getLabelsTable().comparatorProperty());
        contentPaneController.getLabelsTable().setItems(filteredData);

    historyItems = searchPaneController.getHistoryTable().getItems();
    Button clearHistoryButton = searchPaneController.getClearHistoryButton();

    clearHistoryButton.setOnAction(event->{
        playHistory.clear();
        historyItems.setAll(playHistory);

    });

        Button saveHistoryButton = searchPaneController.getSaveHistoryButton();

        saveHistoryButton.setOnAction(event->{
            Mp3Parser.saveHistory(playHistory);


        });

        Button readHistoryButton = searchPaneController.getLoadHistoryButton();

        readHistoryButton.setOnAction(event-> {

            Mp3Parser.readHistory(playHistory);
            historyItems.setAll(playHistory);

        });

    }

    private void configureMenu(){

        MenuItem openSong = menuPaneController.getOpenSongButton();

        openSong.setOnAction(event->{

           FileChooser fileChooser = new FileChooser();
           fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("MP3","*.mp3"));
           File file = fileChooser.showOpenDialog(new Stage());

           try{
                if(!contentPaneController.getLabelsTable().getColumns().contains(file.getName()))
               items.add(createSong(file));

           } catch (org.farng.mp3.TagException e) {
               e.printStackTrace();
           } catch (java.io.IOException e) {
               e.printStackTrace();
           }


        });

        MenuItem openDirectory = menuPaneController.getOpenDirectoryButton();
        openDirectory.setOnAction(event->{

            DirectoryChooser directoryChooser = new DirectoryChooser();
            File dir = directoryChooser.showDialog(new Stage());


            try{
              items.addAll(createMp3SongList(dir));

            }catch(java.io.IOException| org.farng.mp3.TagException| UnsupportedOperationException e){

                e.printStackTrace();

            }});


        }

    private void playSong(int index){

        Mp3Parser.updateHistory(searchPaneController.getHistoryTable(), playHistory, historyItems, items,index);
        mp3Player.loadSong(index);
        controlPaneController.getPlayButton().setSelected(TRUE);
        configureTimeSlider();
        configureVolume();

    }

    private void configureHistoryTable(){

        searchPaneController.getHistoryTable().addEventHandler(MouseEvent.MOUSE_CLICKED, event ->
        {if(event.getClickCount()==2){
            mp3Player.stop();
            Mp3Song mp;
           if( items.contains(mp = historyItems.get(searchPaneController.getHistoryTable().getSelectionModel().getSelectedIndex()))){
                lastIndex = items.lastIndexOf(mp);

                playSong(lastIndex);

            }else{

               items.add(mp);
               playSong(items.lastIndexOf(mp));
           } }});
    }











    }




