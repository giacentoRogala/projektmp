package pl.jacek.mp3player.controller;

import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;

public class MenuPaneController {


    @FXML
    private MenuItem openSongButton;

    @FXML
    private MenuItem openDirectoryButton;

    @FXML
    private MenuItem aboutItem;


    public MenuItem getOpenSongButton(){

        return openSongButton;
    }

    public MenuItem getOpenDirectoryButton(){

        return openDirectoryButton;
    }




}
