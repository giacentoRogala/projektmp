package pl.jacek.mp3player.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Label;
import javafx.beans.binding.*;
import javafx.scene.control.*;

public class ControlPaneController {

    @FXML
    private Button previousButton;

    @FXML
    private ToggleButton playButton;

    @FXML
    private Button nextButton;

    @FXML
    private Slider timeSlider;

    @FXML
    private Slider volumeSlider;

    @FXML
    private Label volumeLabel;
    @FXML
    private Label timeLabel;

    @FXML
    private TextField messageField;

    public TextField getMessageField(){
        return messageField;
    }

    public Slider getVolumeSlider(){

        return volumeSlider;
    }

    public Slider getTimeSlider(){

        return timeSlider;
    }

    public ToggleButton getPlayButton(){

        return playButton;
    }

    public Button getPreviousButton(){

        return previousButton;
    }
    public Button getNextButtonButton(){

        return nextButton;
    }


    public Label getvolumeLabel(){

        return volumeLabel;


    }
    public Label getTimeLabel(){

        return timeLabel;
    }
    public void initialize(){


        volumeSlider.valueProperty().addListener((arg, oldValue, newValue)->{
          double intValue  = (double)newValue*100;
            volumeLabel.setText(String.valueOf((int)intValue));

        });

        //StringBinding volumeSliderBinding = volumeSlider.valueProperty().asString();
        //volumeLabel.textProperty().bind(volumeSliderBinding);


    }







}
