package pl.jacek.mp3player.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView;
import pl.jacek.mp3player.mp3.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;

public class SearchPaneController {



    @FXML
    private TextField searchField;

    @FXML
    private Button saveHistoryButton;

    @FXML
    private Button loadHistoryButton;
    @FXML
    private Button clearHistoryButton;

    @FXML
    private TableView<Mp3Song> historyTable;


    public void initialize(){


        configureHistoryTable();

    }


    public TextField getSearchField(){


        return searchField;

    }

    public Button getClearHistoryButton(){

        return clearHistoryButton;
    }

    public Button getSaveHistoryButton(){
         return saveHistoryButton;
    }

    public Button getLoadHistoryButton(){
        return loadHistoryButton;

    }

    public TableView<Mp3Song> getHistoryTable(){

        return historyTable;
    }


    private void configureHistoryTable(){
        TableColumn<Mp3Song, String> titleColumn = new TableColumn<>("Title");
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        historyTable.getColumns().add(titleColumn);
    }
}
