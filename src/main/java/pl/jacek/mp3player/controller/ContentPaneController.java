package pl.jacek.mp3player.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import pl.jacek.mp3player.mp3.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;


public class ContentPaneController {

    @FXML
    private TableView<Mp3Song> labelsTable;


public TableView<Mp3Song> getLabelsTable(){

    return labelsTable;
}
    public void initialize(){


        configureTableColumns();

    }




    public void configureTableColumns(){

        TableColumn<Mp3Song, String> titleColumn = new TableColumn<>("Title");
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        TableColumn<Mp3Song, String> authorColumn = new TableColumn<>("Author");
        authorColumn.setCellValueFactory(new PropertyValueFactory<>("author"));
        TableColumn<pl.jacek.mp3player.mp3.Mp3Song, String> albumColumn = new TableColumn<>("Album");
        albumColumn.setCellValueFactory(new PropertyValueFactory<>("album"));

        labelsTable.getColumns().add(titleColumn);
        labelsTable.getColumns().add(authorColumn);
        labelsTable.getColumns().add(albumColumn);


    }


    }

