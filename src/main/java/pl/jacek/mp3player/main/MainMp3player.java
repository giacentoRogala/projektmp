package pl.jacek.mp3player.main;

import javafx.fxml.*;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.layout.*;
import javafx.scene.*;
import javafx.stage.*;
import java.io.*;

public class MainMp3player extends Application {



    @Override
    public void start(Stage stage) throws IOException {

      BorderPane borderPane = FXMLLoader.load(getClass().getResource("/mainPane.fxml"));
        Scene scene = new Scene(borderPane);
        stage.setScene(scene);
        stage.setTitle("Mp3 Player");

        stage.show();


    }
}




